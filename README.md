# Melco Test Automation Assignment #

This is a repository for Melco Test Automation Assignment.

## Prerequisite ##
- Install [Node.js](https://nodejs.org/en/)


## Installation ##
1. Clone the repository

2. Install required package and libraries
```
npm install
```

## Steps to test ##
- Run test on command line
```
npm run test
```
- Run test on UI and then select the test
```
npx cypress open
```
<div align="center">
    <img src="https://i.ibb.co/mGJ8s5T/Screenshot-2021-11-02-at-6-24-20-PM.png" alt="Screenshot of User Interface of Cypress" />
</div>

## Results ##
If you run the test through command line, you can get the test result report in cypress/results/mochareports/report.html .

<div align="center">
    <img src="https://i.ibb.co/nbJsH5h/Screenshot-2021-11-02-at-6-06-51-PM.png" alt="Screenshot of results" />
</div>


## Folder/File Setup ##
1. fixture folder: Store test data
2. integration folder: Store test case scipts. If there are changes in test case or you want to add new test case, please add here (filename.spec.js). 
3. pageObjects folder: There is pageObjects folder in integration folder which store any page object you want, e.g. input box, list, button, etc.
4. results folder: Store test results. By default, results folder will automatically clear before next test run
5. plugin folder: Store plugins. For this assignment, there is no usage.
6. support folder: index.js stores global configuration. command.js stores custom commands / overwrite existing commands.
7. cypress.json: Store project configuration, e.g. baseUrl. For configuration setup, please find [here](https://docs.cypress.io/guides/references/configuration#cypress-json)

## Test Cases ##
Please find Melco Test Cases.xlsx
