// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import ItemListPage from '../integration/pageObjects/itemListPage'

Cypress.Commands.add('createDefaultTodos', function() {
    const itemListPage = new ItemListPage()

    cy.fixture('todos').as('todoItems')

    cy.fixture('todos').then((todos) => {
        itemListPage.newItemInput().type(todos.firstInput).type('{enter}')
        itemListPage.newItemInput().type(todos.secondInput).type('{enter}')
    })
})