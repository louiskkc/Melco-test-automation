class itemListPage {
    newItemInput() {
        return cy.get('input[class="new-todo"]')
    }

    toggleAll() {
        return cy.get('label[for="toggle-all"]')
    }

    todoList() {
        return cy.get('ul[class="todo-list"]')
    }

    item() {
        return cy.get('li>div>label')
    }

    // The input box after double click
    itemInput() {
        return cy.get('input[class="edit"]')
    }

    removeItemIcon() {
        return cy.get('button[class="destroy"]')
    }

    checkbox() {
        return cy.get('input[class="toggle"]')
    }

    footer() {
        return cy.get('footer[class="footer"]')
    }

    todoCount() {
        return cy.get('span[class="todo-count"]').get('strong')
    }

    clearCompletedButton() {
        return cy.get('button[class="clear-completed"]')
    }

    tab() {
        return cy.get("")
    }
}

export default itemListPage