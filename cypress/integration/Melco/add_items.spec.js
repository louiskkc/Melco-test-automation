import ItemListPage from '../pageObjects/itemListPage'

describe("Add Items in All list", function() {
    // Get page object
    const itemListPage = new ItemListPage()

    before(function() {
        // Visit todo list website
        // expect 200 status code
        cy.visit('/examples/react/#/')

        // Check if there is NO todo list (as it is fresh)
        itemListPage.todoList().should('not.exist')
    })

    // Setup test data for the test case
    beforeEach(function() {
        cy.fixture('todos').as('todoItems')
    })

    it("Input and save first item", function() {
        // Input new item
        itemListPage.newItemInput().type(this.todoItems.firstInput).type('{enter}')

        itemListPage.todoList().should('be.visible')

        // Check if first todo item created in the todo list
        itemListPage.todoList().within(($list) => {
            itemListPage.item().eq(0).should('have.text', this.todoItems.firstInput)
        })

        // Check if newItemInput box has clear input
        itemListPage.newItemInput().should('have.text', '')

        // Check if only 1 item left
        itemListPage.todoCount().should('have.text', "1")
        
    })

    it("Input and save second item", function() {
        // Input new item
        itemListPage.newItemInput().type(this.todoItems.secondInput).type('{enter}')

        itemListPage.todoList().should('be.visible')

        // Check if todo item created in the todo list
        itemListPage.todoList().within(($list) => {
            itemListPage.item().eq(1).should('have.text', this.todoItems.secondInput)
        })
        
        // Check if newItemInput box has clear input
        itemListPage.newItemInput().should('have.text', '')
        
        // Check if only 1 item left
        itemListPage.todoCount().should('have.text', "2")
    })
})