import ItemListPage from '../pageObjects/itemListPage'

describe("Edit Items", function() {
    // Get page object
    const itemListPage = new ItemListPage()

    before(function() {
        // Visit todo list website
        // expect 200 status code
        cy.visit('/examples/react/#/')

        // Create default todos before testing edit function
        cy.createDefaultTodos().as('todos')

        // Reload the website without clearing cache to simulate existing todo list
        cy.reload()
    })

    // Setup test data for the test case
    let todoItems
    beforeEach(function() {
        cy.fixture('todos').as('todoItems')
    })

    it("Edit and save first item", function() {
        // Update first item

        // Double click to change to input box
        itemListPage.item().eq(0).dblclick()
        
        // Clear existing input and then input new item name
        itemListPage.itemInput().eq(0).clear().type(this.todoItems.firstChangeInput).type('{enter}')

        // Check if first todo item amended in the todo list
        itemListPage.todoList().within(($list) => {
            itemListPage.item().eq(0).should('have.text', this.todoItems.firstChangeInput)
        })
    })

    it("Edit and save second item", function() {
        //Update second item
        
        // Double click to change to input box
        itemListPage.item().eq(1).dblclick()

        // Clear existing input and then input new item name
        itemListPage.itemInput().eq(1).clear().type(this.todoItems.secondChangeInput).type('{enter}')

        // Check if first todo item amended in the todo list
        itemListPage.todoList().within(($list) => {
            itemListPage.item().eq(1).should('have.text', this.todoItems.secondChangeInput)
        })
    })

    it("Check if there are still 2 items left", function() {
        // Check if only 1 item left
        itemListPage.todoCount().should('have.text', "2")
    })
})